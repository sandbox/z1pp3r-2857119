<?php

namespace Drupal\related_menu\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * A block for displaying the selected related menu.
 *
 * @Block(
 *   id = "related_menu",
 *   admin_label = @Translation("Related Menu"),
 *   category = @Translation("Menu Blocks")
 * )
 */
class RelatedMenu extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return ['label_display' => FALSE];
  }

  /**
   * Block constructor.
   *
   * @return array
   *   Render array of data to be passed to the theme layer.
   */
  public function build() {

    // Get the current path and the first element.
    $current_path = \Drupal::service('path.current')->getPath();
    $result = \Drupal::service('path.alias_manager')->getAliasByPath($current_path);
    $path_items = explode("/", $result);
    $uri = $path_items[1];
    $menu_map = json_decode(\Drupal::state()->get('related_menu_mappings'), TRUE);
    $menu_tree = \Drupal::menuTree();
    $menu_name = (isset($menu_map[$uri]) || isset($menu_map[$uri . '/*'])) ? $menu_map[$uri] : \Drupal::state()->get('related_menu_default_menu');
    // Build the typical default set of menu tree parameters.
    $parameters = $menu_tree->getCurrentRouteMenuTreeParameters($menu_name);
    $tree = $menu_tree->load($menu_name, $parameters);
    $manipulators = array(
      // Only show links that are accessible for the current user.
      array('callable' => 'menu.default_tree_manipulators:checkAccess'),
      // Use the default sorting of menu links.
      array('callable' => 'menu.default_tree_manipulators:generateIndexAndSort'),
    );
    $tree = $menu_tree->transform($tree, $manipulators);
    $menu = $menu_tree->build($tree);

    return array('#markup' => render($menu));

  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
     return 0;
  }
}
