<?php

namespace Drupal\related_menu\Form;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\system\Entity\Menu;

/**
 * RelatedMenuSettings class extending FormBase.
 */
class RelatedMenuSettings extends FormBase {

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'related_menu_settings';
  }

  /**
   * Form constructor.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $menus = self::getAvailableMenus();
    $available_menus = (\Drupal::state()->get('related_menu_available_menus'));

    $form['defaults']['related_menu_default_menu'] = array(
      '#type' => 'select',
      '#title' => $this->t('Default sub-menu'),
      '#description' => t('The default menu to use if not otherwise spcified'),
      '#options' => $menus,
      '#default_value' => \Drupal::state()->get('related_menu_default_menu'),
      '#required' => TRUE,
    );

    $form['choose_table_group'] = array(
      '#type' => 'details',
      '#title' => $this->t('Select menus to use'),
      '#open' => FALSE,
    );


    $form['choose_table_group']['related_menu_available_menus'] = array(
      '#type' => 'checkboxes',
      '#options' => $menus,
      '#title' => $this->t('What menus do you want to control?'),
      '#default_value' => \Drupal::state()->get('related_menu_available_menus'),
    );


    $form['available_menus'] = array(
      '#type' => 'fieldset',
      '#title' => $this->t('Available Menus'),
      '#collapsed' => FALSE,
      '#collapsible' => FALSE,
    );


    foreach($menus as $id => $name) {
      if($available_menus[$id]) {
        $form['available_menus']['related_menu_' . $id] = array(
          '#type' => 'textarea',
          '#title' => $this->t('What URI\'s to map to the menu ' . $name),
          '#default_value' => \Drupal::state()->get('related_menu_' . $id),
          '#required' => FALSE,
        );
      }
    }


    $form['buttons']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Submit'),
    );

    return $form;
  }


  /**
   * Form validation handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {


    return $form;
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $menus = self::getAvailableMenus();
    $available_menus = (\Drupal::state()->get('related_menu_available_menus'));

    $relations = array();
    // set default value.
    \Drupal::state()->set('related_menu_default_menu', $form_state->getValues()['related_menu_default_menu']);
    \Drupal::state()->set('related_menu_available_menus', $form_state->getValues()['related_menu_available_menus']);



    foreach($menus as $id => $name) {
      // break up the lines for the menu into an array for easier processing.
      $rows = preg_split("/\\r\\n|\\r|\\n/", $form_state->getValues()['related_menu_' . $id]);
      if($available_menus[$id]) {
        foreach ($rows as $row) {
          \Drupal::state()
            ->set('related_menu_' . $id, $form_state->getValues()['related_menu_' . $id]);
          if ($form_state->getValues()['related_menu_' . $id] != '') {
            $relations[$row] = $id;
          }
        }
      }
    }

    //save JSON representation of the settings.
    \Drupal::state()->set('related_menu_mappings', json_encode($relations, JSON_UNESCAPED_SLASHES));
    drupal_set_message(t('Related menu settings updated'));
  }

  private function getAvailableMenus() {
    $all_menus = Menu::loadMultiple();
    $menus = array();
    $menus[0] = "none"; // default.
    foreach ($all_menus as $id => $menu) {
      $menus[$id] = $menu->label();

    }
    return $menus;
  }

}
